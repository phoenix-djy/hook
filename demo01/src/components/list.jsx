import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
const H1 = styled.ul`
  font-size: 1.5em;
  text-align: center;
  color: ${props => (props.redFont ? 'red' : 'black')};
`
function TodoForm({ addTodo }) {
  const [value, setValue] = useState('')
  const handleSubmit = e => {
    e.preventDefault()
    if (!value) return
    addTodo(value)
    setValue('')
  }
  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={value}
        onChange={e => setValue(e.target.value)}
      />
    </form>
  )
}
function App(props) {
  const [todos, setTodos] = useState([])
  const addTodo = text => {
    const newTodos = [...todos, { text }]
    setTodos(newTodos)
  }
  const completeTodo = index => {
    const newTodos = [...todos]
    newTodos[index].isCompleted = true
    setTodos(newTodos)
  }
  const removeTodo = index => {
    const newTodos = [...todos]
    newTodos.splice(index, 1)
    setTodos(newTodos)
  }
  const Todo = ({ todo, index }) => (
    <li style={{ textDecoration: todo.isCompleted ? 'line-through' : '' }}>
      {todo.text}
      <button onClick={() => completeTodo(index)}>已完成</button>
      <button onClick={() => removeTodo(index)}>删除</button>
    </li>
  )
  return (
    <div>
      <TodoForm addTodo={addTodo} />
      <H1>
        {todos.map((item, index) => (
          <Todo key={index} index={index} todo={item} />
        ))}
      </H1>
    </div>
  )
}

export default App
