import React from 'react'
import { HashRouter, Route, Switch } from 'react-router-dom'
import Home from '../components/input'
import Detail from '../components/list'

const BasicRoute = () => (
  <HashRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/detail" component={Detail} />
    </Switch>
  </HashRouter>
)

export default BasicRoute
